"use strict";
const router = require("express").Router();
const {getClientCredentials, authenticate} = require("../app/Middleware");

//get controller
const passwordController = require("../app/passwords/PasswordController");
const passwordValidator = require("../app/passwords/PasswordValidator");



router.use(getClientCredentials);
router.get("/reset", passwordController.sendResetCode);
router.post("/reset", passwordValidator.reset, passwordController.reset);
router.post("/change", authenticate, passwordValidator.change, passwordController.change);


module.exports = router;