"use strict";
const express = require("express");
const router = express.Router();

//get controller
const permissionController = require("../app/permissions/PermissionController");

router.get("/", permissionController.fetch);
module.exports = router;

