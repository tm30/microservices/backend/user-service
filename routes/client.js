const router = require("express").Router();
const {
  authenticate,
  getClientCredentials,
  onlyMaster,
} = require("../app/Middleware");

//get controller
const clientController = require("../app/clients/ClientController");
const clientValidator = require("../app/clients/ClientValidator");

// router.post("/password/create", decodeHeader, authValidator.savePassword, authController.savePassword);
// router.post("/token/refresh", authController.refresh);
// router.get("/verification/email/resend", authController.resendVerificationEmail);
// router.get("/reset/password",authController.reset);

// router.use(getClientCredentials);
router.post("/", authenticate, clientValidator.create, clientController.create);
router.post(
  "/create",
  authenticate,
  clientValidator.create,
  clientController.create
);
router.get("/", authenticate, onlyMaster, clientController.fetch);
router.post("/verify" /*, authenticate, onlyMaster*/, clientController.verify);
router.post(
  "/allowedServices",
  authenticate,
  clientValidator.allowedServices,
  clientController.addClientServices
);
router.get(
  "/allowedServices",
  authenticate,
  clientController.getClientServices
);
router.delete(
  "/allowedServices",
  authenticate,
  clientValidator.deleteService,
  clientController.deleteClientServices
);
router.put("/:clientId", authenticate, clientController.update);
router.get("/fetch", onlyMaster, clientController.fetch);

router.get("/reset", clientController.sendResetCode);
router.post("/reset", clientValidator.reset, clientController.reset);
router.post(
  "/change",
  authenticate,
  clientValidator.change,
  clientController.change
);

module.exports = router;
