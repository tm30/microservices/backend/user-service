"use strict";
const router = require("express").Router();
const {getClientCredentials} = require("../../app/Middleware");

//get controller
const verificationController = require("../../app/verification/VerificationController");
const verificationValidator = require("../../app/verification/VerificationValidator");



router.post("/", verificationValidator.verifyByType, verificationController.verifyByType);
router.post("/code", verificationValidator.verifyCodeByType, verificationController.verifyCode);

module.exports = router;