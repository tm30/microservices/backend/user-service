'use strict';

const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const schema = mongoose.Schema({
  clientId: {type: String, required: true},
  secret: {type:String, required: true},
  email: {type:String, required: true},
  password: {type:String, required:true},
  meta: {type:Object}
},{
  toJSON: {
    transform: (doc, ret)=>{
      ret.authId = ret._id;
      delete ret.password;
      delete ret.__v;
      delete ret._id;
    }
  },
  timestamps: true
});

schema.plugin(mongoosePaginate);
module.exports = mongoose.model("client-auth", schema);
