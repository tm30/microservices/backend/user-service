// noinspection DuplicatedCode

"use strict";
const { EVENT } = require("tm-constants");
const bcrypt = require("bcryptjs");
const debug = require("debug")("app:debug");
const clientRepository = require("./ClientRepository");
const { CACHE_KEY, EVENTS } = require("../Constants");
const serviceService = require("../services/ServiceService");
const clientEvents = require("../../events/ClientEvent");
const randomString = require("crypto-random-string");
const clientAuthRepository = require("./ClientAuthRepository");

const { formatPhoneNumber } = require("tm-utils");

const authRepository = require("../auths/AuthRepository");

const verifyInterface = require("../verification/ClientVerificationInterface");
/**
 * Create a Client on the platform
 * @param req
 * @param res
 * @param next
 * @return {Promise<*>}
 */
exports.create = async (req, res, next) => {
  let {
    email,
    name,
    logo,
    appURL,
    countryCode,
    password,
    allowedServices = [],
    changePasswordOnLogin = false,
    userRequiredFields = [],
    ...payload
  } = req.body;

  password =
    password || (changePasswordOnLogin ? randomString({ length: 12 }) : "");
  const clientId = await clientRepository.generateClientId();
  const secret = await clientRepository.generateSecret();

  // use the first email for authentication
  if (!Array.isArray(email)) {
    email = [email];
  }

  const clientAuth = await clientAuthRepository.create({
    clientId,
    email: email[0], // Use only the first email for authentication
    password: bcrypt.hashSync(password, 10),
    secret,
  });

  console.log(
    "====================Client Auth created=================",
    clientAuth
  );

  if (!clientAuth)
    return createErrorResponse(
      res,
      "Unable to create client, try again later",
      419
    );

  if (Array.isArray(allowedServices) && allowedServices.length === 0) {
    allowedServices.push("payment-service");
    allowedServices.push("wallet-service");
  }

  let client = await clientRepository.create({
    createdBy: res.user.userId,
    clientId,
    secret,
    appURL,
    email,
    logo,
    name,
    countryCode,
    allowedServices,
    userRequiredFields,
    meta: payload,
  });

  if (!client)
    return createErrorResponse(
      res,
      "Unable to create client at this moment. Please try again",
      500
    );
  debug("==================Generating Client Events====================");
  clientEvents.emit(EVENT.CLIENT.CREATED, client.toJSON());
  return createSuccessResponse(res, client);
};

exports.fetch = async (req, res, next) => {
  const { page, limit, ...query } = req.query;
  const clients = await clientRepository.all(query || {}, {}, page, limit);
  return createSuccessResponse(res, clients);
};

exports.verify = async (req, res, next) => {
  const { clientId } = req.body;
  console.log("Request to verify client, ", clientId);
  let client = await clientRepository.findById(clientId);
  if (!client) return createSuccessResponse(res, { data: "Client Not Found" });
  console.log("verified client", client);
  return createSuccessResponse(res, client);
};

async function normalizeServices(allowedServices) {
  if (!Array.isArray(allowedServices)) {
    allowedServices = [allowedServices];
  }

  // remove duplicate services
  allowedServices = Array.from(new Set(allowedServices));
  // check if services are valid
  const allowedClientServices = (await serviceService.fetchServices()).map(
    (service) => service.key
  );

  const unsupportedService = allowedServices.some(
    (service) => !allowedClientServices.includes(service)
  );
  return { allowedServices, unsupportedService };
}

/**
 * Add or update client allowed services
 * @param req
 * @param res
 * @param next
 * @return {Promise<*>}
 */
exports.addClientServices = async (req, res, next) => {
  let { clientId, secret, allowedServices, ...payload } = req.body;
  let client = await clientRepository.findOne({ clientId });
  if (!client) return createErrorResponse(res, "Client Not Found", 404);

  const CLIENT_KEY = `${clientId}_CLIENT`;

  // check if allowed services is an array
  const normalizedServices = await normalizeServices(allowedServices);
  allowedServices = normalizedServices.allowedServices;
  const unsupportedService = normalizedServices.unsupportedService;

  if (unsupportedService) {
    createErrorResponse(res, "One of the services is not allowed", 404);
  }

  const update = {};
  update.allowedServices = allowedServices;
  await client.update(update);
  client = await clientRepository.findOne({ clientId });

  // Override client details in cache with updated ones
  cache.setAsync(CLIENT_KEY, JSON.stringify(client.toJSON()));

  return createSuccessResponse(res, client, 200);
};

exports.getClientServices = async (req, res, next) => {
  // let { clientId, secret, allowedServices, ...payload } = req.body;
  let client = await clientRepository.findOne({
    clientId: req.params.clientId,
  });
  if (!client) return createErrorResponse(res, "Client Not Found", 404);
  const clientServices = client.allowedServices;
  return createSuccessResponse(res, clientServices, 200);
};

exports.deleteClientServices = async (req, res, next) => {
  let { clientId, secret, allowedServices, ...payload } = req.body;
  let client = await clientRepository.findOne({
    clientId: req.params.clientId,
  });
  if (!client) return createErrorResponse(res, "Client Not Found", 404);
  const CLIENT_KEY = `${clientId}_CLIENT`;
  const normalizedServices = await normalizeServices(allowedServices);
  allowedServices = normalizedServices.allowedServices;
  const unsupportedService = normalizedServices.unsupportedService;

  if (unsupportedService) {
    return createErrorResponse(
      res,
      "One of the services is not supported",
      404
    );
  }

  const clientServices = client.allowedServices;
  if (clientServices.length == 0)
    return createErrorResponse(
      res,
      "Client doesn't have access to any service",
      404
    );

  const update = {};
  // check if the client already has the service and remove it
  allowedServices.forEach((service) => {
    if (clientServices.includes(service)) {
      let index = clientServices.indexOf(service);
      clientServices.splice(index, 1);
    }
  });

  update.allowedServices = clientServices;
  await client.update(update);
  client = await clientRepository.findOne({ clientId });
  client = { ...client.toJSON() };
  const response = {
    status: true,
    message: "Service removed",
  };

  // Override client details in cache with updated ones
  cache.setAsync(CLIENT_KEY, JSON.stringify(client));
  return createSuccessResponse(res, client.allowedServices);
};

function normalizeEmail(email, payload) {
  if (Array.isArray(email)) {
    if (Array.isArray(payload.email)) {
      email = email.concat(payload.email);
      return Array.from(new Set(email));
    } else {
      email.unshift(payload.email);
      return Array.from(new Set(email));
    }
  } else {
    return payload.email;
  }
}

exports.update = async (req, res, next) => {
  let { clientId, secret, ...payload } = req.body;
  let client = await clientRepository.findOne({ clientId });
  console.log("Clients", client);
  if (!client) return createErrorResponse(res, "Client Not Found", 404);

  const CLIENT_KEY = `${clientId}_CLIENT`;
  let meta = { ...client.meta };
  const update = {};
  for (let c in payload) {
    if (!payload.hasOwnProperty(c)) continue;
    if (clientRepository.getNonMetaFields().includes(c)) {
      update[c] = payload[c];
      continue;
    }

    meta[c] = payload[c];
  }
  update.meta = meta;
  await client.update(update);
  client = { ...client.toJSON(), ...update };

  clientEvents.emit(EVENT.CLIENT.UPDATED, client);
  cache.setAsync(CLIENT_KEY, JSON.stringify(client));
  return createSuccessResponse(res, client);
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

exports.sendResetCode = async (req, res) => {
  let { type, value, clientId } = req.query;
  const payload = {
    type: "email" || type, // only email is supported for now, flip the or statement to make it dynamic
    value,
    clientId,
  };

  const query = { clientId: clientId };
  if (type == "email") query.email = value;
  if (type == "phone")
    query.phoneNumber = formatPhoneNumber(value, res.countryCode);

  let auth = await clientAuthRepository.findOne(query);
  if (!auth) {
    const client = await clientRepository.findOne(query);
    console.log("Client was found, ", client);
    if (!client)
      return createErrorResponse(
        res,
        `Account Attached to the ${type} Not Found`,
        404
      );
    await clientAuthRepository.create({
      ...query,
      secret: client.secret,
      password: bcrypt.hashSync(randomString({ length: 12 }), 10),
    });
  }
  const client = await clientRepository.findOne(query);
  const validationError = await verifyInterface[type].send.validate(payload);
  if (validationError) return createErrorResponse(res, validationError, 422);

  const { error, data } = await verifyInterface[type].send.execute(
    {
      ...payload,
      client,
    },
    client,
    "password reset"
  );
  // console.log("Reset Log", error, data);
  if (error) return createErrorResponse(res, error);
  return createSuccessResponse(
    res,
    `A ${
      !auth.password ? "verification" : "password reset"
    } code has been sent to the email/phone number provided`
  );
};

exports.reset = async (req, res) => {
  let { type, value, code, clientId, password } = req.body;
  const payload = {
    value,
    code,
    clientId,
  };
  const query = { clientId: clientId };
  if (type == "email") query.email = value;
  if (type == "phone")
    query.phoneNumber = formatPhoneNumber(value, res.countryCode);

  let auth = await clientAuthRepository.findOne(query);
  if (!auth)
    return createErrorResponse(
      res,
      `Account Attached to the ${type} Not Found`,
      404
    );

  const validationError = await verifyInterface[type].verify.validate(payload);
  if (validationError) return createErrorResponse(res, validationError, 422);

  const { error, errorCode, data } = await verifyInterface[type].verify.execute(
    payload
  );
  console.log("Verify Log", error, errorCode, data);
  if (error) return createErrorResponse(res, error, errorCode || 500);

  auth.password = bcrypt.hashSync(password, 10);
  await auth.save();
  return createSuccessResponse(res, "Password reset successfully");
};

/**
 * Save A New password after resetting
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
exports.change = async (req, res) => {
  let payload = req.body;
  res.user.password = bcrypt.hashSync(payload.password, 10);
  await authRepository.update(
    { userId: res.user.userId },
    { password: res.user.password }
  );
  // audit.trail("You changed your password",
  //     "Password Changed",
  //     res.auth
  // );
  return createSuccessResponse(res, "Password Changed Successfully");
};
