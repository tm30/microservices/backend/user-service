const clientAuthModel = require("./ClientAuthModel")
const Repository = require("../MongoDBRepository");
class ClientAuthRepository extends Repository {
    constructor() {
        super(clientAuthModel);
    }
}
module.exports = (new ClientAuthRepository());