"use strict";
const Joi = require("@hapi/joi");
const { validate } = require("../helper");

const clientRepository = require("./ClientRepository");
exports.create = async (req, res, next) => {
  const schema = {
    name: Joi.string().required(),
    email: Joi.array().items(Joi.string().email().required()).required(),
    password: Joi.string().required(),
  };

  const error = validate(schema, req.body);
  if (error) return createSuccessResponse(res, error, 422);

  //checkout if this user has created another service with the same name
  let check = await clientRepository.findOne({ name: req.body.name });
  if (check)
    return createErrorResponse(
      res,
      "A Client with the same name exists!!!",
      419
    );

  return next();
};

exports.allowedServices = async (req, res, next) => {
  const schema = {
    clientId: Joi.string().required(),
    allowedServices: Joi.array().items(Joi.string().required()),
  };

  const error = validate(schema, req.body);
  if (error) return createSuccessResponse(res, error, 422);
  return next();
};

exports.deleteService = async (req, res, next) => {
  const schema = {
    clientId: Joi.string().required(),
    secret: Joi.string().required(),
    allowedServices: Joi.array().items(Joi.string().required()),
  };

  const error = validate(schema, req.body);
  if (error) return createSuccessResponse(res, error, 422);

  let check = await clientRepository.findOne({ clientId: req.body.clientId });
  console.log("check: ", check);
  if (check.secret !== req.body.secret)
    return createErrorResponse(res, "Client's secrets don't match", 419);
  return next();
};

exports.reset = async (req, res, next) => {
  const schema = {
    type: Joi.string().required(),
    code: Joi.string().required(),
    password: Joi.string().required(),
  };

  const result = validate(schema, req.body);
  if (result) return createErrorResponse(res, result, 422);

  return next();
};
exports.change = async (req, res, next) => {
  const { email, phoneNumber } = req.body;
  const schema = {
    password: Joi.string().required(),
  };

  const result = validate(schema, req.body);
  if (result) return createErrorResponse(res, result, 422);
  //
  // res.auth = await authRepository.findOne({
  //     clientId: res.clientId, $or: [{email}, {phoneNumber}]
  // });
  //
  // if (!res.auth)
  //     return createErrorResponse(res, "Account Attached to email address/phone number not found", 404);

  return next();
};
