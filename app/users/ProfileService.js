"use strict";
const moment = require("moment");
const {ES_INDEX} = require("../Constants");
const profileSearchRepository = require("./ProfileSearchRepository");
exports.save = async (body, operation = "create") => {
    try {
        const esPayload = {
            id: body.userId,
            operation,
            index: ES_INDEX.USERS,
            elasticsearchUrl: process.env.ELASTICSEARCH_URL,
            body
        };
        console.log("ESPayload", process.env.ELASTICSEARCH_RUNNER_QUEUE, process.env.RABBITMQ_URL);
        const queueResponse = await Queue.queue(process.env.ELASTICSEARCH_RUNNER_QUEUE, esPayload, {
            persistent: true,
            priority: 10
        });
        console.log("Queue ES Response", queueResponse);
        return queueResponse;
    } catch (e) {
        console.log("Error", e);
        return e;
    }
};


exports.searchUsingElasticsearch = async ({dateTo, dateFrom, keyword, ...query}, options = {}) => {
    try {
        const must = profileSearchRepository.appendMultiplePropertyMatch(query);
        if (dateFrom) {
            dateFrom = moment(dateFrom).startOf("day").format("x");
            dateTo = moment(dateTo).endOf("day").format("x");
            must.push(profileSearchRepository.appendDateRange("createdAt", dateFrom, dateTo));
        }

        let body = {
            query: {
                bool: {
                    must
                }
            }
        };

        console.log("Body", JSON.stringify(body));
        const count = await profileSearchRepository.count(body);
        body._source = ["userId"];
        body.sort = [
            {createdAt: {order: "desc"}}
        ];

        if (options.page) {
            body.from = ((options.page - 1) * options.limit);
            body.size = options.limit;
        }


        const results = await profileSearchRepository.search(body);
        return {
            data: results.data.map(result => result.userId),
            total: count?.body?.count || results.total
            // data: results

        }

    } catch (e) {
        console.log("Error", e);
        logException(e, {dateTo, dateFrom, keyword, ...query, options});
        return {error: e.message};
    }
};
