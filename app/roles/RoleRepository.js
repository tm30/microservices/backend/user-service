"use strict";

const Role = require('./RoleModel');
const Repository = require("../MongoDBRepository");

class RoleRepository extends Repository{
    constructor(){
        super(Role);
    }
}
module.exports = (new RoleRepository());